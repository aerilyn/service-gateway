package app

import (
	"gitlab.com/aerilyn/service-library/middleware/auth"
	"gitlab.com/aerilyn/service-library/middleware/header"
	"gitlab.com/aerilyn/service-library/middleware/log"
	"gitlab.com/aerilyn/service-library/otel/newrelic"
)

type RequiredMiddlewares struct {
	NewRelic *newrelic.NewReliMiddlewareRegistry
	Header   *header.HeaderMiddlewareRegistry
	Logging  *log.LoggingMiddlewareRegistry
	Auth     *auth.AuthMiddlewareRegistry
}
