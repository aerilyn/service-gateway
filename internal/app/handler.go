package app

import (
	authenticationkweb "gitlab.com/aerilyn/service-gateway/internal/app/authentication/delivery/web"
	healthcheckweb "gitlab.com/aerilyn/service-gateway/internal/app/healthcheck/delivery/web"
	itemweb "gitlab.com/aerilyn/service-gateway/internal/app/item/delivery/web"
	wishlistweb "gitlab.com/aerilyn/service-gateway/internal/app/wishlist/delivery/web"
)

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry    *healthcheckweb.HealthCheckHandlerRegistry
	AuthenticationHTTPHandlerRegistry *authenticationkweb.AuthenticationHandlerRegistry
	ItemHTTPHandlerRegistry           *itemweb.ItemHandlerRegistry
	WishlistHTTPHandlerRegistry       *wishlistweb.WishlistHandlerRegistry
}
