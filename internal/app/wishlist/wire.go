package wishlist

import (
	"gitlab.com/aerilyn/service-gateway/internal/app/wishlist/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.WishlistRegistryOptions), "*"),
	web.NewWishlistHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet()
