package web

import (
	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/httpRequest"
)

type WishlistRegistryOptions struct {
	HttpRequestHelper httpRequest.HttpRequestGatewayHelper
	Config            config.Config
}

type WishlistHandlerRegistry struct {
	Options WishlistRegistryOptions
}

func NewWishlistHandlerRegistry(options WishlistRegistryOptions) *WishlistHandlerRegistry {
	return &WishlistHandlerRegistry{
		Options: options,
	}
}

func (h *WishlistHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/wishlist", func(r chi.Router) {
		r.Get("/", WebHandler(h.Options))
		r.Post("/", WebHandler(h.Options))
		r.Delete("/", WebHandler(h.Options))
	})
	return nil
}
