package web

import (
	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/httpRequest"
)

type AuthenticationHandlerOptions struct {
	Config            config.Config
	HttpRequestHelper httpRequest.HttpRequestGatewayHelper
}

type AuthenticationHandlerRegistry struct {
	Opts AuthenticationHandlerOptions
}

func NewAuthenticationHandlerRegistry(opts AuthenticationHandlerOptions) *AuthenticationHandlerRegistry {
	return &AuthenticationHandlerRegistry{
		Opts: opts,
	}
}

func (h *AuthenticationHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Post("/user/", WebHandler(h.Opts))
	r.Get("/user/", WebHandler(h.Opts))
	r.Post("/signin/", WebHandler(h.Opts))
	return nil
}
