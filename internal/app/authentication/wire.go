package authentication

import (
	"gitlab.com/aerilyn/service-gateway/internal/app/authentication/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.AuthenticationHandlerOptions), "*"),
	web.NewAuthenticationHandlerRegistry,
)

var validatorModuleSet = wire.NewSet()

var usecaseModuleSet = wire.NewSet()
