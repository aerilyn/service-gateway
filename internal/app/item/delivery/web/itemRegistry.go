package web

import (
	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/httpRequest"
)

type ItemRegistryOptions struct {
	HttpRequestHelper httpRequest.HttpRequestGatewayHelper
	Config            config.Config
}

type ItemHandlerRegistry struct {
	Options ItemRegistryOptions
}

func NewItemHandlerRegistry(options ItemRegistryOptions) *ItemHandlerRegistry {
	return &ItemHandlerRegistry{
		Options: options,
	}
}

func (h *ItemHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/admin", func(r chi.Router) {
		r.Route("/item", func(r chi.Router) {
			r.Group(func(r chi.Router) {
				r.Post("/", WebHandler(h.Options))
			})
		})
	})
	r.Route("/item", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Get("/{id}/", WebHandler(h.Options))
		})
	})
	r.Route("/items", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Get("/", WebHandler(h.Options))
		})
	})
	return nil
}
