package web

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/aerilyn/service-gateway/internal/pkg/config"
	"gitlab.com/aerilyn/service-library/errors"
)

func WebHandler(opts ItemRegistryOptions) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		resBody := make(map[string]interface{})
		response, err := opts.HttpRequestHelper.Request(r, opts.Config.Get(config.HostItem))
		if response != nil {
			defer response.Body.Close()
		}
		if err != nil {
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, err.Values())
			return
		}
		if errDecoder := json.NewDecoder(response.Body).Decode(&resBody); errDecoder != nil {
			err = errors.NewInternalSystemError().CopyWith(errors.Message(errDecoder.Error()))
			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, err.Values())
			return
		}
		render.Status(r, response.StatusCode)
		render.JSON(w, r, resBody)
	}
}
