package app

import (
	"context"
	"fmt"
	"net/http"
	"reflect"

	"github.com/go-chi/chi"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	pkgCors "gitlab.com/aerilyn/service-library/middleware/cors"
	"gitlab.com/aerilyn/service-library/module"
	"gitlab.com/aerilyn/service-library/pubsub"
)

type ModuleOptions struct {
	RequiredHandlers
	RequiredMiddlewares
	Config       *config.Config
	PubSubRunner *pubsub.Runner
}

type Module struct {
	*module.ApplicationDelegate
	options ModuleOptions
	router  chi.Router
}

func NewModule(options ModuleOptions) *Module {
	return &Module{
		ApplicationDelegate: module.NewApplicationDelegate(),
		options:             options,
		router:              chi.NewRouter(),
	}
}

func (m *Module) SetRoute(ctx context.Context) error {
	err := m.registerMiddlewares()
	if err != nil {
		return err
	}

	err = m.registerHandlers()
	if err != nil {
		return err
	}

	m.router, err = m.ExecuteApplyRegistry(m.router)
	if err != nil {
		return err
	}

	return nil
}

func (m *Module) AddPubsubRunner() error {
	err := m.AddRunner(m.options.PubSubRunner)
	if err != nil {
		return errors.NewInternalSystemError().CopyWith(errors.Message(err.Error()))
	}
	return nil
}

func (m *Module) registerHandlers() error {
	refHandlers := reflect.ValueOf(m.options.RequiredHandlers)
	for i := 0; i < refHandlers.NumField(); i++ {
		switch h := interface{}(refHandlers.Field(i).Interface()).(type) {
		case module.HTTPHandlerRegistry:
			err := m.AddHTTPHandlerRegistry(h)
			if err != nil {
				return err
			}
			break
		case pubsub.HandlerRegistry:
			m.options.PubSubRunner.AddHandlerRegistry(h)
			break
		}

	}
	return nil
}

func (m *Module) registerMiddlewares() error {
	refMiddlewares := reflect.ValueOf(m.options.RequiredMiddlewares)
	for i := 0; i < refMiddlewares.NumField(); i++ {
		switch md := interface{}(refMiddlewares.Field(i).Interface()).(type) {
		case module.HTTPMiddlewareRegistry:
			err := m.ApplicationDelegate.AddHTTPMiddlewareRegistry(md)
			if err != nil {
				return err
			}
			break
		case pubsub.HandlerMiddlewaresRegistry:
			m.options.PubSubRunner.AddHandlerMiddlewaresRegistry(md)
			break
		}
	}
	return nil
}

func (m *Module) RunServer() {
	port := fmt.Sprintf(":%s", m.options.Config.Get(config.SERVICE_PORT))
	fmt.Println("listen to ", m.options.Config.Get(config.SERVICE_PORT))
	httpCors := pkgCors.SetCors().Handler(m.router)
	err := http.ListenAndServe(port, httpCors)
	if err != nil {
		panic(err.Error())
	}

}
