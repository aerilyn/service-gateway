//go:build wireinject
// +build wireinject

package app

import (
	"gitlab.com/aerilyn/service-gateway/internal/app/authentication"
	"gitlab.com/aerilyn/service-gateway/internal/app/healthcheck"
	"gitlab.com/aerilyn/service-gateway/internal/app/item"
	"gitlab.com/aerilyn/service-gateway/internal/app/wishlist"
	"gitlab.com/aerilyn/service-gateway/internal/pkg/helper"
	providerHelper "gitlab.com/aerilyn/service-gateway/internal/pkg/helper/usecase"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/cryptography"
	"gitlab.com/aerilyn/service-library/cryptography/aes256"
	pkgMD5 "gitlab.com/aerilyn/service-library/hash/md5"
	"gitlab.com/aerilyn/service-library/httpRequest"
	httpRequestUsecase "gitlab.com/aerilyn/service-library/httpRequest/usecase"
	"gitlab.com/aerilyn/service-library/jwt"
	"gitlab.com/aerilyn/service-library/middleware/auth"
	"gitlab.com/aerilyn/service-library/middleware/header"
	"gitlab.com/aerilyn/service-library/middleware/log"
	"gitlab.com/aerilyn/service-library/module"
	"gitlab.com/aerilyn/service-library/otel/newrelic"
	"gitlab.com/aerilyn/service-library/pubsub"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	wire.Struct(new(RequiredHandlers), "*"),
	wire.Struct(new(RequiredMiddlewares), "*"),
	wire.Struct(new(ModuleOptions), "*"),
	wire.Struct(new(aes256.Aes256Opts), "*"),
	wire.Struct(new(auth.AuthMiddlewareOpts), "*"),
	wire.Struct(new(header.HeaderMiddlewareOpts), "*"),
	wire.Struct(new(providerHelper.HttpHelperOpts), "*"),
	wire.Bind(new(helper.HttpHelper), new(*providerHelper.HttpHelper)),
	wire.Bind(new(httpRequest.HttpRequestGatewayHelper), new(*httpRequestUsecase.HttpRequestGatewayHelper)),
	wire.Bind(new(jwt.Jwt), new(*jwt.JwtOpts)),
	wire.Bind(new(config.ConfigEnv), new(*config.Config)),
	wire.Bind(new(cryptography.Cryptography), new(*aes256.Aes256)),
	wire.Bind(new(pkgMD5.HashMD5), new(*pkgMD5.HashMD5Impl)),
	NewModule,
	aes256.ProvideAes256,
	auth.NewAuthMiddlewareRegistry,
	config.InjectConfig,
	config.ProvideConfig,
	header.NewHeaderMiddlewareRegistry,
	httpRequestUsecase.NewHttpRequestGatewayHelper,
	jwt.ProvideJwt,
	log.NewLoggingMiddlewareRegistry,
	module.NewApplicationDelegate,
	newrelic.ProvideNewRelic,
	newrelic.NewNewReLicMiddlewareRegistry,
	pkgMD5.NewHashMD5,
	providerHelper.NewHttpHelper,
	pubsub.NewRunner,
	pubsub.ProvideRouter,
	pubsub.ProvideLoggerWithStdLogger,
)

func InjectApp() (*Module, func(), error) {
	panic(wire.Build(
		ModuleSet,
		authentication.ModuleSet,
		healthcheck.ModuleSet,
		item.ModuleSet,
		wishlist.ModuleSet,
	))
}
