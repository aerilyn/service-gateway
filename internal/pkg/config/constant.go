package config

const (
	HostWishlist       string = "HOST_WISHLIST"
	HostItem           string = "HOST_ITEM"
	HostAuthentication string = "HOST_AUTHENTICATION"
)
