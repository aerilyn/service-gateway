package usecase

import (
	"fmt"
	"net/http"

	"gitlab.com/aerilyn/service-gateway/internal/pkg/helper"
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/middleware"
)

type HttpHelperOpts struct {
	Config config.Config
}

type HttpHelper struct {
	options HttpHelperOpts
}

func NewHttpHelper(opts HttpHelperOpts) *HttpHelper {
	return &HttpHelper{
		options: opts,
	}
}

func (x *HttpHelper) GetHttpValue(r *http.Request, host string) helper.HttpValueDto {
	urlPath := host + r.URL.Path
	rawQuery := r.URL.RawQuery
	if rawQuery != "" {
		urlPath = fmt.Sprintf("%s?%s", urlPath, rawQuery)
	}
	return helper.HttpValueDto{
		Platform: r.Header.Get(middleware.PlatformKey),
		Method:   r.Method,
		UrlPath:  urlPath,
		Auth:     r.Header.Get("Authorization"),
	}
}
