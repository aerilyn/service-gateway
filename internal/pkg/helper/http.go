package helper

import "net/http"

type HttpValueDto struct {
	Platform string `json:"platform"`
	Method   string `json:"method"`
	UrlPath  string `json:"urlPath"`
	Auth     string `json:"authorization"`
}

type HttpHelper interface {
	GetHttpValue(r *http.Request, host string) HttpValueDto
}
